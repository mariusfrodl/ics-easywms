using abmsoft.mobile.core.Interfaces;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZXing.Mobile;

[assembly: Xamarin.Forms.Dependency(typeof(DroidScanner))]
namespace abmsoft.mobile.core.Interfaces
{
    public class DroidScanner : IScanner
    {
        public async Task<ScanResult> Scan()
        {
            var context =  Forms.Context;
            var scanner = new MobileBarcodeScanner()
            {
                UseCustomOverlay = false,
                BottomText = "automatisches Scannen",
                TopText = "Falls unscharf bitte aufs Bild dr�cken.",
                FlashButtonText = "Blitz",
                CameraUnsupportedMessage = "Fehler beim Scannen aufgetreten.",
            };

            MobileBarcodeScanningOptions opt = new MobileBarcodeScanningOptions();
    
            try
            {
                var result = await scanner.Scan(context, opt);

                return new ScanResult
                {
                    Text = result.Text,
                };
            }
            catch (System.Exception)
            {

                return new ScanResult
                {
                    Text = string.Empty,
                };
            }
        }
    }
}