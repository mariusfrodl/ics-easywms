﻿using System;
using Autofac;
using ICS_easyWMS.Bootstrap;
using ICS_easyWMS.Droid.Services;
using abmsoft.Mobile.Core.Interfaces;
using abmsoft.Mobile.Core.Services;


namespace ICS_easyWMS.Droid
{
	public class DroidBootstrapper  : Bootstrapper
	{
		public DroidBootstrapper(Xamarin.Forms.Application application) : base(application)
		{
       }


        protected override void ConfigureContainer(ContainerBuilder builder)
		{
			base.ConfigureContainer(builder);

            // Register platform specific types
            //			builder.RegisterType<AzureBackendService> ().As<IBackendService> ().SingleInstance ();
        }
    }
}

