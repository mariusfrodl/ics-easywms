using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using abmsoft.mobile.core.Layouts;
using System.Timers;
using Stradivari_Mobile.Droid.Renderers;
using System.ComponentModel;
using Android.Views;
using System.Reflection;
using Java.Lang;
using Android.Graphics;
using Android.Widget;
using Android.Views.InputMethods;

[assembly: ExportRenderer(typeof(SIPDisabledEntry), typeof(SIPDisabledEntryRenderer))]
namespace Stradivari_Mobile.Droid.Renderers
{
    public class SIPDisabledEntryRenderer : EntryRenderer
    {
        private int teste;

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                ((SIPDisabledEntry)e.NewElement).PropertyChanging += OnPropertyChanging;
                ((SIPDisabledEntry)e.NewElement).PropertyChanged += Changed;

                ((SIPDisabledEntry)e.NewElement).Completed += CompletedEvent;
               
                this.Control.Touch += touchevnts;

                //this.Control.FocusChange += Control_FocusChange;
                //this.Control.Click += Control_Click;

            }

            if (e.OldElement != null)
            {
                ((SIPDisabledEntry)e.OldElement).PropertyChanging -= OnPropertyChanging;
                //((SIPDisabledEntry)e.NewElement).Completed -= CompletedEvent;
                // this.Control.Touch -= touchevnts;
            }

            // Disable the Keyboard on Focus
            this.Control.ShowSoftInputOnFocus = false;
        }

        private void Changed(object sender, PropertyChangedEventArgs e)
        {
            //var textfeld = (Entry)sender;
            
            //if(textfeld.IsEnabled == false && textfeld.Text != "")
            //{
            //    SetColors();
            //}
        }

        private void Control_Click(object sender, EventArgs e)
        {
            if (Control.HasFocus)
            {
                Control.SetSelection(Control.Text.Length);
            }
        }

        private void Control_FocusChange(object sender, FocusChangeEventArgs e)
        {
            if (e.HasFocus)
            {
                Control.SetSelection(Control.Text.Length);
            }
        }


        private void CompletedEvent(object sender, EventArgs e)
        {
            SetColors();
        }
        private void SetColors()
        {
            Control.SetTextColor(Element.IsEnabled ? Element.TextColor.ToAndroid() : Android.Graphics.Color.Black);
            Control.SetBackgroundColor(Element.IsEnabled ? Element.BackgroundColor.ToAndroid() : Android.Graphics.Color.LightGray);
        }

        private void touchevnts(object sender, TouchEventArgs e)
        {
                //Entry et = sender as Entry;
                //et.Text = "";
         
                InputMethodManager imm = (InputMethodManager)this.Context.GetSystemService(Android.Content.Context.InputMethodService);
                imm.ShowSoftInput(this.Control, ShowFlags.Implicit);        
          
        }

        private void OnPropertyChanging(object sender, Xamarin.Forms.PropertyChangingEventArgs propertyChangingEventArgs)
        {
            //SetColors();
            // Check if the view is about to get Focus
            if (propertyChangingEventArgs.PropertyName == VisualElement.IsFocusedProperty.PropertyName)
            {
                // incase if the focus was moved from another Entry
                // Forcefully dismiss the Keyboard 
                InputMethodManager imm = (InputMethodManager)this.Context.GetSystemService(Android.Content.Context.InputMethodService);
                imm.HideSoftInputFromWindow(this.Control.WindowToken, 0);
            }

        }
    }
}