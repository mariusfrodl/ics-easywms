﻿using Autofac;
using Xamarin.Forms;
using abmsoft.Mobile.Core.Interfaces;
using ICS_easyWMS.Bootstrap;

namespace ICS_easyWMS.iOS.Bootstrap
{
	public class IosBootstrapper : Bootstrapper
	{
		public IosBootstrapper(Xamarin.Forms.Application application) : base(application)
		{
		}


		protected override void ConfigureContainer(ContainerBuilder builder)
		{
			base.ConfigureContainer(builder);

          //  builder.RegisterType<DBPath>().As<IDBPath>();

        }
    }
}

