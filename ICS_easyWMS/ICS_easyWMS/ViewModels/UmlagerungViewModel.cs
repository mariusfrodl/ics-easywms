﻿using abmsoft.Mobile.Core.ViewModels;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using abmsoft.Mobile.Core.Interfaces;
using XLabs.Forms.Mvvm;
using ICS_easyWMS.Views;
using abmsoft.Mobile.Core.Models.Database;
using System.Collections.ObjectModel;
using abmsoft.Mobile.Core.Services;
using abmsoft.mobile.core.Interfaces;
using SQLite;
using System.Windows.Input;

namespace ICS_easyWMS.ViewModels
{
    [ViewType(typeof(UmlagerungView))]
    class UmlagerungViewModel : ViewModelBase
    {
        INavigator _navigationService;
        DataAccessService _dbService;
        public ObservableCollection<EntryProperty> ListEntryProperty { get; set; }
        public ObservableCollection<Umlagerung> ListUmlagerungen { get; set; }

        public ICommand SpeichernCommand { private set; get; }
        public ICommand LoeschenCommand { private set; get; }
        public ICommand NeuCommand { private set; get; }
        public ICommand ScanLidVonCommand { private set; get; }
        public ICommand ScanArtikelCommand { private set; get; }
        public ICommand ScanLidZuCommand { private set; get; }
        public ICommand ZurueckCommand { private set; get; }

        Func<string, UmlagerungViewModel> _UmlagerungViewModelFactory;
        public UmlagerungViewModel(INavigator navigationService, Func<string, UmlagerungViewModel> UmlagerungViewModelFactory)
        {
            _navigationService = navigationService;
            _dbService = new DataAccessService();
            _UmlagerungViewModelFactory = UmlagerungViewModelFactory;
            ListEntryProperty = new ObservableCollection<EntryProperty>();
            ListUmlagerungen = new ObservableCollection<Umlagerung>();

            TitleUntermenue = "Umlagerung";
            MeldungTextColor = "Red";

            DemoValue = "Demo";
 
            FillListEntryProperty();

            SpeichernCommand = new Command(() =>
            {
                Save();
            });

            NeuCommand = new Command(() =>
            {
                try
                {
                    SelectedUL = null;
                    FillListUmlagerungen();
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            LoeschenCommand = new Command(async () =>
            {
                try
                {
                    if (SelectedUL != null)
                    {
                        string result = await App.Current.MainPage.DisplayActionSheet("Wollen Sie wirklich den Datensatz löschen?", "Nein", "Ja");

                        if (result == "Ja")
                        {
                            _dbService.DeleteRow(SelectedUL);
                            SelectedUL = null;
                            FillListUmlagerungen();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ScanArtikelCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    ArtikelValue = result.Text;
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ScanLidVonCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    LidVonValue = result.Text;
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ScanLidZuCommand = new Command(async () =>
            {
                try
                {
                    var result = await DependencyService.Get<IScanner>().Scan();
                    LidZuValue = result.Text;
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });

            ZurueckCommand = new Command(async () =>
            {
                try
                {
                    await _navigationService.PopAsync();
                }
                catch (Exception ex)
                {
                    MeldungTextColor = "Red";
                    Meldung = ex.Message;
                }
            });
        }

        public bool IsNumeric(string input)
        {
            int test;
            return int.TryParse(input, out test);
        }

        private void FillListUmlagerungen()
        {
            try
            {
                List<Umlagerung> items = _dbService.QueryAllUmlagerung();

                ListUmlagerungen.Clear();

                foreach (Umlagerung item in items)
                {
                    item.Schriftgroesse = ListViewZeileSchriftgroesse;

                    ListUmlagerungen.Add(item);
                }

            }
            catch (Exception ex)
            {
                MeldungTextColor = "Red";
                Meldung = ex.Message;
            }
        }

        private void FillListEntryProperty()
        {
            try
            {
                List<EntryProperty> items = _dbService.QueryAllEntryProperty("4");

                ListEntryProperty.Clear();

                foreach (EntryProperty item in items)
                {
                    ListEntryProperty.Add(item);
                }

                if (ListEntryProperty.Count > 0)
                {
                    InitDisplay();
                }
            }
            catch (Exception ex)
            {
                MeldungTextColor = "Red";
                Meldung = ex.Message;
            }
        }

        private void InitDisplayFelder()
        {
            int height = App.screenHeight;
            int width = App.screenWidth;

            ObererRand = (float)(height * 0.057);
            Leerzeile = (float)(height * 0.049);
            Eingabe1 = (float)(height * 0.065);
            Eingabe2 = (float)(height * 0.065);
            Eingabe3 = (float)(height * 0.065);
            Eingabe4 = (float)(height * 0.065);
            Meldungzeile = (float)(height * 0.049);

            if (height >= 1200)
            {
                Schrift = 34;
                ListViewZeileSchriftgroesse = 24;
                ListViewZeilenhoehe = 34;
                Buttonzeile = (float)(height * 0.05);
                UntererRand = (float)(height * 0.057);
            }
            else
            {
                Schrift = 16;
                ListViewZeileSchriftgroesse = 12;
                ListViewZeilenhoehe = 22;
                Buttonzeile = (float)(height * 0.064);
                UntererRand = (float)(height * 0.097);
            }

        }

        private void InitDisplay()
        {
            foreach (EntryProperty item in ListEntryProperty)
            {
                switch (item.FeldID)
                {
                    case "0":
                        LidVonVisibility = item.Visibility;
                        LidVonPlaceholder = item.Bezeichnung;
                        if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
                        {
                            LidVonBarcodeVisibility = false;
                        }
                        else
                        {
                            LidVonBarcodeVisibility = item.Visibility;
                        }
                        break;
                    case "1":
                        ArtikelVisibility = item.Visibility;
                        ArtikelPlaceholder = item.Bezeichnung;
                        if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
                        {
                            ArtikelBarcodeVisibility = false;
                        }
                        else
                        {
                            ArtikelBarcodeVisibility = item.Visibility;
                        }
                        break;
                    case "2":
                        StueckVisibility = item.Visibility;
                        StueckPlaceholder = item.Bezeichnung;
                        StueckBarcodeVisibility = false;
                        break;
                    case "3":
                        LidZuVisibility = item.Visibility;
                        LidZuPlaceholder = item.Bezeichnung;
                        if ((Device.OS == TargetPlatform.Windows) || (!_dbService.GetEinstellungKamera()))
                        {
                            LidZuBarcodeVisibility = false;
                        }
                        else
                        {
                            LidZuBarcodeVisibility = item.Visibility;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private string _meldung;
        public string Meldung
        {
            get { return _meldung; }
            set
            {
                SetProperty(ref _meldung, value, "Meldung");
            }
        }

        private double _Schrift;
        public double Schrift
        {
            get { return _Schrift; }
            set
            {
                SetProperty(ref _Schrift, value, "Schrift");
            }
        }

        private float _ObererRand;
        public float ObererRand
        {
            get { return _ObererRand; }
            set
            {
                SetProperty(ref _ObererRand, value, "ObererRand");
            }
        }

        private float _Leerzeile;
        public float Leerzeile
        {
            get { return _Leerzeile; }
            set
            {
                SetProperty(ref _Leerzeile, value, "Leerzeile");
            }
        }

        private float _Eingabe1;
        public float Eingabe1
        {
            get { return _Eingabe1; }
            set
            {
                SetProperty(ref _Eingabe1, value, "Eingabe1");
            }
        }

        private float _Eingabe2;
        public float Eingabe2
        {
            get { return _Eingabe2; }
            set
            {
                SetProperty(ref _Eingabe2, value, "Eingabe2");
            }
        }

        private float _Eingabe3;
        public float Eingabe3
        {
            get { return _Eingabe3; }
            set
            {
                SetProperty(ref _Eingabe3, value, "Eingabe3");
            }
        }

        private float _Eingabe4;
        public float Eingabe4
        {
            get { return _Eingabe4; }
            set
            {
                SetProperty(ref _Eingabe4, value, "Eingabe4");
            }
        }

        private float _Meldungzeile;
        public float Meldungzeile
        {
            get { return _Meldungzeile; }
            set
            {
                SetProperty(ref _Meldungzeile, value, "Meldungzeile");
            }
        }

        private float _Buttonzeile;
        public float Buttonzeile
        {
            get { return _Buttonzeile; }
            set
            {
                SetProperty(ref _Buttonzeile, value, "Buttonzeile");
            }
        }

        private double _ListViewZeileSchriftgroesse;
        public double ListViewZeileSchriftgroesse
        {
            get { return _ListViewZeileSchriftgroesse; }
            set
            {
                SetProperty(ref _ListViewZeileSchriftgroesse, value, "ListViewZeileSchriftgroesse");
            }
        }

        private float _ListViewZeilenhoehe;
        public float ListViewZeilenhoehe
        {
            get { return _ListViewZeilenhoehe; }
            set
            {
                SetProperty(ref _ListViewZeilenhoehe, value, "ListViewZeilenhoehe");
            }
        }

        private float _UntererRand;
        public float UntererRand
        {
            get { return _UntererRand; }
            set
            {
                SetProperty(ref _UntererRand, value, "UntererRand");
            }
        }

        private string _meldungtextcolor;
        public string MeldungTextColor
        {
            get { return _meldungtextcolor; }
            set
            {
                SetProperty(ref _meldungtextcolor, value, "MeldungTextColor");
            }
        }

        private string _TitleUntermenue;
        public string TitleUntermenue
        {
            get { return _TitleUntermenue; }
            set
            {
                SetProperty(ref _TitleUntermenue, value, "TitleUntermenue");
            }
        }

        private string _Artikelnummer;
        public string ArtikelValue
        {
            get { return _Artikelnummer; }
            set
            {
                SetProperty(ref _Artikelnummer, value);
            }
        }

        private string _LagerplatzVon;
        public string LidVonValue
        {
            get { return _LagerplatzVon; }
            set
            {
                SetProperty(ref _LagerplatzVon, value);
            }
        }

        private string _strMenge;
        public string StueckValue
        {
            get { return _strMenge; }
            set
            {
                SetProperty(ref _strMenge, value);
            }
        }

        private string _LagerplatzZu;
        public string LidZuValue
        {
            get { return _LagerplatzZu; }
            set
            {
                SetProperty(ref _LagerplatzZu, value);
            }
        }

        private string _PickerSelItem;
        public string PickerSelItem
        {
            get { return _PickerSelItem; }
            set
            {
                SetProperty(ref _PickerSelItem, value);
            }
        }

        private string _LidZuPlaceholder;
        public string LidZuPlaceholder
        {
            get { return _LidZuPlaceholder; }
            set
            {
                SetProperty(ref _LidZuPlaceholder, value, "LidZuPlaceholder");
            }
        }

        private string _ArtikelPlaceholder;
        public string ArtikelPlaceholder
        {
            get { return _ArtikelPlaceholder; }
            set
            {
                SetProperty(ref _ArtikelPlaceholder, value, "ArtikelPlaceholder");
            }
        }

        private string _StueckPlaceholder;
        public string StueckPlaceholder
        {
            get { return _StueckPlaceholder; }
            set
            {
                SetProperty(ref _StueckPlaceholder, value, "StueckPlaceholder");
            }
        }

        private string _LidVonPlaceholder;
        public string LidVonPlaceholder
        {
            get { return _LidVonPlaceholder; }
            set
            {
                SetProperty(ref _LidVonPlaceholder, value, "LidVonPlaceholder");
            }
        }

        private bool _LidVonVisibility;
        public bool LidVonVisibility
        {
            get { return _LidVonVisibility; }
            set
            {
                SetProperty(ref _LidVonVisibility, value, "LidVonVisibility");
            }
        }

        private bool _LidVonBarcodeVisibility;
        public bool LidVonBarcodeVisibility
        {
            get { return _LidVonBarcodeVisibility; }
            set
            {
                SetProperty(ref _LidVonBarcodeVisibility, value, "LidVonBarcodeVisibility");
            }
        }

        private bool _ArtikelVisibility;
        public bool ArtikelVisibility
        {
            get { return _ArtikelVisibility; }
            set
            {
                SetProperty(ref _ArtikelVisibility, value, "ArtikelVisibility");
            }
        }

        private bool _ArtikelBarcodeVisibility;
        public bool ArtikelBarcodeVisibility
        {
            get { return _ArtikelBarcodeVisibility; }
            set
            {
                SetProperty(ref _ArtikelBarcodeVisibility, value, "ArtikelBarcodeVisibility");
            }
        }

        private bool _StueckVisibility;
        public bool StueckVisibility
        {
            get { return _StueckVisibility; }
            set
            {
                SetProperty(ref _StueckVisibility, value, "StueckVisibility");
            }
        }

        private bool _StueckBarcodeVisibility;
        public bool StueckBarcodeVisibility
        {
            get { return _StueckBarcodeVisibility; }
            set
            {
                SetProperty(ref _StueckBarcodeVisibility, value, "StueckBarcodeVisibility");
            }
        }

        private string _DemoValue;
        public string DemoValue
        {
            get { return _DemoValue; }
            set
            {
                SetProperty(ref _DemoValue, value, "DemoValue");
            }
        }

        private bool _LidZuVisibility;
        public bool LidZuVisibility
        {
            get { return _LidZuVisibility; }
            set
            {
                SetProperty(ref _LidZuVisibility, value, "LidZuVisibility");
            }
        }

        private bool _LidZuBarcodeVisibility;
        public bool LidZuBarcodeVisibility
        {
            get { return _LidZuBarcodeVisibility; }
            set
            {
                SetProperty(ref _LidZuBarcodeVisibility, value, "LidZuBarcodeVisibility");
            }
        }

        private Umlagerung valSelectedUL;

        public Umlagerung SelectedUL
        {
            get
            {
                return valSelectedUL;
            }
            set
            {
                if (value != valSelectedUL)
                {
                    valSelectedUL = value;

                    if (value != null)
                    {
                        LidVonValue = value.Quelllagerplatz;
                        ArtikelValue = value.Artikelnummer;
                        StueckValue = value.Menge.ToString();
                        LidZuValue = value.Ziellagerplatz;
                    }
                    else
                    {
                        LidVonValue = string.Empty;
                        ArtikelValue = string.Empty;
                        StueckValue = string.Empty;
                        LidZuValue = string.Empty;
                    }
                }
            }
        }

        private void Save()
        {
            try
            {
                Meldung = string.Empty;
                int anz = _dbService.QueryAllUmlagerung().Count;

                if (anz >= 5)
                {
                    MeldungTextColor = "Red";
                    Meldung = "Demo-Version: Anzahl erreicht.";
                    return;
                }

                Umlagerung row = new Umlagerung();

                if (LidVonVisibility && (string.IsNullOrEmpty(_LagerplatzVon)))
                {
                    MeldungTextColor = "Red";
                    Meldung = LidVonPlaceholder + " eingeben";
                    return;
                }
                if (ArtikelVisibility && (string.IsNullOrEmpty(_Artikelnummer)))
                {
                    MeldungTextColor = "Red";
                    Meldung = ArtikelPlaceholder + " eingeben";
                    return;
                }
                if (StueckVisibility && (string.IsNullOrEmpty(_strMenge)))
                {
                    MeldungTextColor = "Red";
                    Meldung = StueckPlaceholder + " eingeben";
                    return;
                }
                else
                {
                    if (!(IsNumeric(_strMenge)))
                    {
                        MeldungTextColor = "Red";
                        Meldung = StueckPlaceholder + " als Zahl eingeben";
                        return;
                    }
                }
                if (LidZuVisibility && (string.IsNullOrEmpty(_LagerplatzZu)))
                {
                    MeldungTextColor = "Red";
                    Meldung = LidZuPlaceholder + " eingeben";
                    return;
                }

                row.Benutzer = GlobalData.Global.Benutzer;
                row.Artikelnummer = _Artikelnummer;
                row.Quelllagerplatz = _LagerplatzVon;
                row.Ziellagerplatz = _LagerplatzZu;
                row.Lager = GlobalData.Global.Lager;
                row.Bereich = GlobalData.Global.Bereich;
                row.Datum = DateTime.Now.ToString();

                if (IsNumeric(_strMenge))
                {
                    row.Menge = Convert.ToDouble(_strMenge);
                }
                else
                {
                    row.Menge = 0;
                }

                if (SelectedUL == null)
                {
                    _dbService.InsertRow(row);
                }
                else
                {
                    row.Positionsnummer = SelectedUL.Positionsnummer;
                    _dbService.UpdateRow(row);
                }

                ArtikelValue = string.Empty;
                LidVonValue = string.Empty;
                StueckValue = string.Empty;
                LidZuValue = string.Empty;
                MeldungTextColor = "#089e70";
                Meldung = "Speichern erfolgreich";

                FillListUmlagerungen();
                SelectedUL = null;
            }
            catch (Exception ex)
            {
                MeldungTextColor = "Red";
                Meldung = ex.Message;
            }
        }

        private Command _ArtikelEntryCommand;
        public Command ArtikelEntryCommand
        {
            get
            {
                this._ArtikelEntryCommand = this._ArtikelEntryCommand ?? new Command<EventArgs>((e) =>
                {
                });
                return this._ArtikelEntryCommand;
            }
        }
        private Command _StueckEntryCommand;
        public Command StueckEntryCommand
        {
            get
            {
                this._StueckEntryCommand = this._StueckEntryCommand ?? new Command<EventArgs>((e) =>
                {
                    Save();
                });
                return this._StueckEntryCommand;
            }
        }
        private Command _LidVonEntryCommand;
        public Command LidVonEntryCommand
        {
            get
            {
                this._LidVonEntryCommand = this._LidVonEntryCommand ?? new Command<EventArgs>((e) =>
                {
                });
                return this._LidVonEntryCommand;
            }
        }
        private Command _LidZuEntryCommand;
        public Command LidZuEntryCommand
        {
            get
            {
                this._LidZuEntryCommand = this._LidZuEntryCommand ?? new Command<EventArgs>((e) =>
                {
                });
                return this._LidZuEntryCommand;
            }
        }

        private Command _appearingCommand;
        public Command AppearingCommand
        {
            get
            {
                this._appearingCommand = this._appearingCommand ?? new Command(() =>
                {
                    if (_dbService.GetDemoVersion())
                    {
                        DemoValue = "Demo";
                    }
                    else
                    {
                        DemoValue = string.Empty;
                    }
                    InitDisplayFelder();
                    FillListUmlagerungen();
                });
                return this._appearingCommand;
            }
        }
    }
}
