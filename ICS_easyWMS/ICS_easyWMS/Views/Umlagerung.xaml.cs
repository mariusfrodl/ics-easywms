﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ICS_easyWMS.Views
{
    public partial class UmlagerungView : ContentPage
    {
        public UmlagerungView()
        {
            InitializeComponent();
            this.Appearing += UmlagerungView_Appearing;
            this.ArtikelEntry.Completed += UmlagerungView_Artikel_Completed;
            this.LidVonEntry.Completed += UmlagerungView_LidVon_Completed;
            this.LidZuEntry.Completed += UmlagerungView_LidZu_Completed;
            this.StueckEntry.Completed += UmlagerungView_Stueck_Completed;
        }
        private void UmlagerungView_Appearing(object sender, EventArgs e)
        {
        }
        private void UmlagerungView_LidVon_Completed(object sender, EventArgs e)
        {
            if (ArtikelEntry.IsVisible)
            {
                ArtikelEntry.Focus();
            }
        }
         private void UmlagerungView_Artikel_Completed(object sender, EventArgs e)
        {
            if (LidZuEntry.IsVisible)
            {
                LidZuEntry.Focus();
            }
        }
        private void UmlagerungView_LidZu_Completed(object sender, EventArgs e)
        {
            if (StueckEntry.IsVisible)
            {
                StueckEntry.Focus();
            }
        }
        private void UmlagerungView_Stueck_Completed(object sender, EventArgs e)
        {
            if (LidVonEntry.IsVisible)
            {
                LidVonEntry.Focus();
            }
        }
    }
}
