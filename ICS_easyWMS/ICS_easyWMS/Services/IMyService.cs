﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICS_easyWMS.Services
{
    interface IMyService
    {
        string GetCorrectAnswer();
    }

    public class MyService : IMyService
    {
        public string GetCorrectAnswer()
        {
            return "42";
        }
           
    }
}
