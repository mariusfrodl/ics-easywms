﻿using System;
using Xamarin.Forms;

namespace abmsoft.Mobile.Core.Interfaces
{
	// Abstraktion des Page-Interfaces für testbare Dialoge
	public interface IPage : IDialogProvider
	{
		INavigation Navigation { get; }
	}	
}

