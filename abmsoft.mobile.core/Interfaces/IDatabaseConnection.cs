﻿using SQLite;

namespace abmsoft.mobile.core.Interfaces
{
    public interface IDatabaseConnection
    {
        SQLiteConnection DbConnection();
        bool ExistsDatabase();
        void DropDatabase();
        string GetDeviceID();
    }
}
