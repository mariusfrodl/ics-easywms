﻿using System;
using System.Windows.Input;

namespace abmsoft.Mobile.Core.Interfaces
{


	public enum LocationType {
		ItemLocation,
		UserLocation
	}

	public interface ILocationViewModel
	{
		int Id { get; set;}
		string Title { get; set; }
		string Description { get; }
		double Latitude { get; }
		double Longitude { get; }
		bool IsValid { get;}
		LocationType Type { get; }
		ICommand Command { get; }
	}
}

