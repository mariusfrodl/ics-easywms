﻿using System;
using System.Collections.Generic;

namespace abmsoft.Mobile.Core.Interfaces
{
	public interface IUserActivityViewModel
	{
	
		string ActivityType { get; set;}
		string ActivityTitle { get; set;}

		Dictionary<string,string> ActivityUserValues { get; set;}

		event EventHandler DataReady;
	}
}

