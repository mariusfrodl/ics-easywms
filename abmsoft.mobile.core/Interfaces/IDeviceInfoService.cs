﻿using abmsoft.mobile.core.Device;
using System.Threading.Tasks;

namespace abmsoft.mobile.core.Interfaces
{
    public interface IDeviceInfoService
    {
        Task<bool> Initialize();
        Task CheckVersion(int currentVersion);
        DeviceInfoKey GetKeyFromString(string key);
        T GetValue<T>(DeviceInfoKey key);
        Task SetValue(DeviceInfoKey key, object value);
    }

    public enum Platform
    {
        Android = 0,
        iOS = 1,
        WindowsPhone = 2,
        Windows = 3
    }
}