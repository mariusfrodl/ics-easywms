﻿using System;
using System.Threading.Tasks;
using System.IO;

namespace abmsoft.Mobile.Core.Interfaces
{
	public interface ISoundProvider
	{
		Task PlaySoundAsync (string filename);
		void StopSound();
	}
}

