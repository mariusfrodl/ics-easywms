﻿using System.Threading.Tasks;

namespace abmsoft.mobile.core.Interfaces
{
    public interface IScanner
    {
        Task<ScanResult> Scan();
    }

    public class ScanResult
    {
        public string Text { get; set; }
    }
}