﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace abmsoft.Mobile.Core.Interfaces
{
	public interface IFileService
	{
		string GetPersonalFolderPath();
		string GetLocalApplicationDataPath ();
		string GetFileName (string filePath);
		Task<string> CopyFileToPersonalFolder (string filePathfrom, string subDir, bool overwrite);
		Task<string> CopyFileToLocalApplicationFolder (string filePathfrom, string subDir, bool overwrite);
		bool FileExists (string filePath);
		Task<bool> DeleteFile (string filePath);
		byte[] ReadByteArrayFromFile (string imageLocation);
		string WriteByteArrayToFile (byte[]bytes, string directory, string fileName);
		List<string> GetFilesInFolder(string directory);
	}
}

