﻿using System;
using System.Windows.Input;

namespace abmsoft.Mobile.Core.Interfaces
{
	public interface IItemViewModel
	{
		string Title { get; set; }

		ICommand Command { get; set; }
	}
}

