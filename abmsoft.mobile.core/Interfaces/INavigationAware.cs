﻿using System;
using System.ComponentModel;

namespace abmsoft.Mobile.Core.Interfaces
{
    public interface INavigationAware
    {
        void NavigatedTo();

        void NavigatedFrom();
    }
}

