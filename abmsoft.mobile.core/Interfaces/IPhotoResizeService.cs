﻿using System;

namespace abmsoft.Mobile.Core.Interfaces
{
	/// <summary>
	/// I photo resize service.
	/// <see cref="https://forums.xamarin.com/discussion/58342/best-way-to-create-thumbnails-of-images"/>
	/// </summary>
	public interface IPhotoResizeService
	{
		byte[] ResizeImage (byte[] imageData, float width, float height, int quality);
	}
}

