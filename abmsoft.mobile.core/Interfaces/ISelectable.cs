﻿using System;
using System.Windows.Input;

namespace abmsoft.Mobile.Core.Interfaces
{
    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }
}

