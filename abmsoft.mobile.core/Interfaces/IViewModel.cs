﻿using System;
using System.ComponentModel;

namespace abmsoft.Mobile.Core.Interfaces
{
	public interface IViewModel : INotifyPropertyChanged, INavigationAware
    {
        string Title { get; set; }

        // void SetState<T>(Action<T> action) where T : class, IViewModel;
    }
}

