﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace abmsoft.Mobile.Core.Extensions
{
    public static class MathExtensions
    {
        public static int Mode(this IEnumerable<int> values)
        {
            return values
                .GroupBy(x => x)
                .OrderByDescending(g => g.Count())
                .Select(g => g.Key)
                .FirstOrDefault();
        }


		public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
		{
			if (val.CompareTo(min) < 0) return min;
			else if(val.CompareTo(max) > 0) return max;
			else return val;
		}

    }
}

