﻿namespace abmsoft.Mobile.Core.Extensions
{
    public enum BezierTangent
    {
        None,
        Normal,
        Reversed
    }
}
