﻿using Xamarin.Forms;
using System.Threading.Tasks;
namespace abmsoft.Mobile.Core.TriggerActions
{

	public class FocusTriggerAction : TriggerAction<View>
	{
		public bool Focused { get; set; }

		protected override async void Invoke (View entry)
		{
			await Task.Delay(200);

			if (Focused)
			{
				entry.Focus();
			}
			else
			{
				entry.Unfocus();
			}
		}
	}

	public class UnfocusedTriggerAction : TriggerAction<View>
	{
		protected override void Invoke (View entry)
		{
			//YourViewModel.ViewModelIsSearchBarFocused = false;
		}
	}
}