﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using abmsoft.mobile.core.Interfaces;
using abmsoft.mobile.core.Interfaces.Database;
using DeviceInfoPlugin = Plugin.DeviceInfo;

using DataTable = abmsoft.mobile.core.Models.WebApi.DataTableExtended;
using DataRow = abmsoft.mobile.core.Models.WebApi.DataRowExtended;

namespace abmsoft.mobile.core.Device
{
    static class DeviceInfoHelper
    {
        public async static Task UpdateValues(IDeviceInfoService devInfo, ILoggingService logging, IDataAccessService appData)
        {
			int deviceInfoVersion = 1;
			
			try
            {
                DeviceInfoPlugin.Abstractions.IDeviceInfo deviceInfoPlugin = DeviceInfoPlugin.CrossDeviceInfo.Current;

                // Prüfung der DeviceInfo-Struktur
                await devInfo.CheckVersion(deviceInfoVersion);

                string deviceDescription = $"{deviceInfoPlugin.Model} ({(Platform)deviceInfoPlugin.Platform} {deviceInfoPlugin.VersionNumber})";

                await devInfo.SetValue(DeviceInfoKey.DeviceInfoVersion, deviceInfoVersion);
                await devInfo.SetValue(DeviceInfoKey.DeviceID, deviceInfoPlugin.Id);
                await devInfo.SetValue(DeviceInfoKey.DeviceName, deviceInfoPlugin.Model);
                await devInfo.SetValue(DeviceInfoKey.PlatformType, (Platform)deviceInfoPlugin.Platform);
                await devInfo.SetValue(DeviceInfoKey.OSVersion, deviceInfoPlugin.VersionNumber.ToString());
                //devInfo.SetValue(DeviceInfoKey.CLRVersion, Environment.Version.ToString());
                await devInfo.SetValue(DeviceInfoKey.DeviceDescription, deviceDescription);

                //string screenSize = Screen.PrimaryScreen.Bounds.Width + " x " + Screen.PrimaryScreen.Bounds.Height;
                //devInfo.SetValue(DeviceInfoKey.ScreenSize, screenSize);

                // App-Infos
                await devInfo.SetValue(DeviceInfoKey.AppName, Global.ApplicationInfo.ExeName);
                //	devInfo.SetValue(DeviceInfoKey.AppPath, abmsoft.Mobile.Tools.Function.GetProgramPath());
                await devInfo.SetValue(DeviceInfoKey.AppVersion, Global.ApplicationInfo.VersionLong);
                await devInfo.SetValue(DeviceInfoKey.AppBuiltTime, Global.ApplicationInfo.BuiltDate.ToString("dd.MM.yyyy HH:mm"));
                await devInfo.SetValue(DeviceInfoKey.AppDbVersion, appData.DbVersionScript);
                await devInfo.SetValue(DeviceInfoKey.AppStartTime, Global.Now);
                await devInfo.SetValue(DeviceInfoKey.AppDataPath, appData.DatabasePath);
                //devInfo.SetValue(DeviceInfoKey.AppDataTotalUsedBytes, DirectorySize(new DirectoryInfo(Global.DataPath), true));

                //int mandantID = Global.AppData.MandantID;
                //if (mandantID != 0)
                //{
                //    devInfo.SetValue(DeviceInfoKey.MandantID, mandantID);
                //}

                // Framework-Infos
                await devInfo.SetValue(DeviceInfoKey.FrameworkName, Global.FrameworkInfo.ExeName);
                await devInfo.SetValue(DeviceInfoKey.FrameworkVersion, Global.FrameworkInfo.VersionLong);
                await devInfo.SetValue(DeviceInfoKey.FrameworkBuiltTime, Global.FrameworkInfo.BuiltDate.ToString("dd.MM.yyyy HH:mm"));

                // Framework-Device-Infos
                //if (DeviceHelper.DeviceAssembly != null)
                //{
                //    AssemblyInfo ai = new AssemblyInfo(DeviceHelper.DeviceAssembly, "FrameworkPPC.Device");
                //    devInfo.SetValue(DeviceInfoKey.FrameworkDeviceFileName, DeviceHelper.DeviceFile);
                //    devInfo.SetValue(DeviceInfoKey.FrameworkDeviceVersion, ai.VersionLong);
                //    devInfo.SetValue(DeviceInfoKey.FrameworkDeviceBuiltTime, ai.BuiltDate.ToString("dd.MM.yyyy HH:mm"));
                //}
                //else
                //{
                //    devInfo.SetValue(DeviceInfoKey.FrameworkDeviceFileName, "-");
                //    devInfo.SetValue(DeviceInfoKey.FrameworkDeviceVersion, "-");
                //    devInfo.SetValue(DeviceInfoKey.FrameworkDeviceBuiltTime, new DateTime(2000, 1, 1));
                //}

                // Datenbank-Infos
                /*
                    ApplicationConfig appConfig = Global.AppConfig;
                devInfo.SetValue(DeviceInfoKey.DbType,Global.AppData.DbEngineName);
                devInfo.SetValue(DeviceInfoKey.DbVersion, Global.AppData.Connection.ServerVersion);
                devInfo.SetValue(DeviceInfoKey.DbCreateTime, Global.AppData.CreateDbDateTime);
				devInfo.SetValue(DeviceInfoKey.DbServer, appConfig.DbServer);
				devInfo.SetValue(DeviceInfoKey.DbName, appConfig.DbName);
				devInfo.SetValue(DeviceInfoKey.DbUsername, appConfig.DbUsername);
				devInfo.SetValue(DeviceInfoKey.DbConnectionString, appConfig.DbConnectionString);
				devInfo.SetValue(DeviceInfoKey.DbVersionLong, Global.AppData.DbEngineVersionLong);
				
				DataTable dtDbInfo = Global.GetDbInfo();
				devInfo.SetValue(DeviceInfoKey.DbFileDataSize, GetDbInfoValue(dtDbInfo, "Size"));
				devInfo.SetValue(DeviceInfoKey.DbFileLogSize, GetDbInfoValue(dtDbInfo, "SizeLog"));
				devInfo.SetValue(DeviceInfoKey.DbRowCountAllTables, GetDbInfoValue(dtDbInfo, "Anzahl Datensätze"));
				devInfo.SetValue(DeviceInfoKey.DbBackupFile, GetDbInfoValue(dtDbInfo, "BackupDB"));
				devInfo.SetValue(DeviceInfoKey.DbRestoreTime, GetDbInfoValue(dtDbInfo, "RestoreDB"));
				devInfo.SetValue(DeviceInfoKey.DbRestoreSyncTime, GetDbInfoValue(dtDbInfo, "RestoreSync"));
                */

                // Telefon-Infos
                await devInfo.SetValue(DeviceInfoKey.PhoneNr, PhoneNr);


                // Kamera vorhanden?
                //if (PlatformDetection.PlatformType != PlatformType.Win32NT)
                //{
                //    bool hasCamera = Microsoft.WindowsMobile.Status.SystemState.CameraPresent;
                //    devInfo.SetValue(DeviceInfoKey.HasCamera, hasCamera);
                //}

                // Arbeitsspeicher
                //if (FrameworkPPC.Tools.PlatformDetection.OSVersion.Platform == PlatformID.WinCE)
                //{
                //    devInfo.SetValue(DeviceInfoKey.MemoryTotalPhysical, String.Format("{0}", MemoryStatus.TotalPhys / (1024 * 1024)));
                //    devInfo.SetValue(DeviceInfoKey.MemoryAvailablePhysical, String.Format("{0}", MemoryStatus.AvailPhys / (1024 * 1024)));
                //    devInfo.SetValue(DeviceInfoKey.MemoryTotalVirtual, String.Format("{0}", MemoryStatus.TotalVirtual / (1024 * 1024)));
                //    devInfo.SetValue(DeviceInfoKey.MemoryAvailableVirtual, String.Format("{0}", MemoryStatus.AvailVirtual / (1024 * 1024)));
                //}

                // Vorhandene Speicherkarten und interne Flashcards incl. freiem Speicher ermitteln
                //UpdateStorageValues();

                // Navigation

            }
            catch (Exception ex)
            {
                logging.Write("DeviceInfoHelper", true, "UpdateValues-Error: " + ex.Message);
            }

        }


        /*private static string GetDbInfoValue(DataTable dt, string property)
		{
			DataRow[] dr = dt.Select("Property = '" + property + "'");

			if (dr.Length > 0)
			{
				return dr[0]["Value"].ToString();
			}
			else
			{
				return "-";
			}
		}
        */

        public static string PhoneNr
        {
            get
            {
                // TODO: Auslesen der Telefon-Nummer
                return "<not available>";
            }
        }

        //public static void UpdatePhoneValues(RasBookEntry conEntry, string imei)
        //{
        //    try
        //    {
        //        DeviceInfo devInfo = Global.DevInfo;

        //        devInfo.SetValue(DeviceInfoKey.ConnectionName, conEntry.EntryName);
        //        devInfo.SetValue(DeviceInfoKey.Provider, conEntry.Provider);
        //        devInfo.SetValue(DeviceInfoKey.PhoneNr, conEntry.PhoneNr);
        //        devInfo.SetValue(DeviceInfoKey.IMSI, conEntry.IMSI);
        //        devInfo.SetValue(DeviceInfoKey.IMEI, imei);
        //        devInfo.SetValue(DeviceInfoKey.APN, conEntry.APN);
        //        devInfo.SetValue(DeviceInfoKey.SimType, Global.AppConfig.SimType);
        //        devInfo.SetValue(DeviceInfoKey.ConnectionUserName, conEntry.Username);
        //        devInfo.SetValue(DeviceInfoKey.ConnectionUserModified, conEntry.UserModified);
        //    }
        //    catch (Exception ex)
        //    {
        //        Global.AppData.SystemLog("DeviceInfoHelper", true, "UpdatePhoneValues-Error: " + ex.Message);
        //    }
        //}

        //public static void UpdateStorageValues()
        //{
        //    try
        //    {
        //        DeviceInfo devInfo = Global.DevInfo;

        //        // StorageRoot
        //        StorageCard.DiskFreeSpace freeSpace = StorageCard.GetDiskFreeSpace(@"\");
        //        devInfo.SetValue(DeviceInfoKey.StorageRootTotalBytes, freeSpace.TotalBytes);
        //        devInfo.SetValue(DeviceInfoKey.StorageRootTotalFreeBytes, freeSpace.TotalFreeBytes);

        //        // StorageCard
        //        string[] storageCards = StorageCard.GetStorageCardNames();
        //        string storageCardName;
        //        int arrayLength = storageCards.GetUpperBound(0) + 1;

        //        for (int i = 1; i <= Math.Min(arrayLength, 4); i++)
        //        {
        //            storageCardName = storageCards[i - 1];
        //            freeSpace = StorageCard.GetDiskFreeSpace(storageCardName);

        //            switch (i)
        //            {
        //                case 1:
        //                    devInfo.SetValue(DeviceInfoKey.StorageCard1Name, storageCardName);
        //                    devInfo.SetValue(DeviceInfoKey.StorageCard1TotalBytes, freeSpace.TotalBytes);
        //                    devInfo.SetValue(DeviceInfoKey.StorageCard1TotalFreeBytes, freeSpace.TotalFreeBytes);
        //                    break;
        //                case 2:
        //                    devInfo.SetValue(DeviceInfoKey.StorageCard2Name, storageCardName);
        //                    devInfo.SetValue(DeviceInfoKey.StorageCard2TotalBytes, freeSpace.TotalBytes);
        //                    devInfo.SetValue(DeviceInfoKey.StorageCard2TotalFreeBytes, freeSpace.TotalFreeBytes);
        //                    break;
        //                case 3:
        //                    devInfo.SetValue(DeviceInfoKey.StorageCard3Name, storageCardName);
        //                    devInfo.SetValue(DeviceInfoKey.StorageCard3TotalBytes, freeSpace.TotalBytes);
        //                    devInfo.SetValue(DeviceInfoKey.StorageCard3TotalFreeBytes, freeSpace.TotalFreeBytes);
        //                    break;
        //                case 4:
        //                    devInfo.SetValue(DeviceInfoKey.StorageCard4Name, storageCardName);
        //                    devInfo.SetValue(DeviceInfoKey.StorageCard4TotalBytes, freeSpace.TotalBytes);
        //                    devInfo.SetValue(DeviceInfoKey.StorageCard4TotalFreeBytes, freeSpace.TotalFreeBytes);
        //                    break;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Global.AppData.SystemLog("DeviceInfoHelper", true, "UpdateStorageCardValues-Error: " + ex.Message);
        //    }
        //}

        //private static long DirectorySize(DirectoryInfo dInfo, bool includeSubDir)
        //{
        //	// Enumerate all the files
        //	long totalSize = dInfo.EnumerateFiles()
        //				 .Sum(file => file.Length);

        //	// If Subdirectories are to be included
        //	if (includeSubDir)
        //	{
        //		// Enumerate all sub-directories
        //		totalSize += dInfo.EnumerateDirectories()
        //				 .Sum(dir => DirectorySize(dir, true));
        //	}
        //	return totalSize;
        //}
    }
}
