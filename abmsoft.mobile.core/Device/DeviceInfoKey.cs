﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace abmsoft.mobile.core.Device
{
    public static class DeviceInfoGroup
    {
        public static string GetGroup(DeviceInfoKey key)
        {
            int id = (int)key;
            if (id < 20) return "System";
            else if (id < 40) return "App";
            else if (id < 50) return "Framework";
            else if (id < 60) return "FrameworkDevice";
			else if (id < 70) return "FrameworkPlatform";
			else if (id < 100) return "Database";
            else if (id < 110) return "Memory";
            else if (id < 130) return "Storage";
            else if (id < 150) return "Phone";
			else if (id < 160) return "System";
			else if (id < 170) return "System";
			else return "Global";
        }
    }

    public enum DeviceInfoKey
    {
        DeviceInfoVersion = 0,
        DeviceName,
        DeviceID,
        PlatformType,
        OSVersion,
        CLRVersion,
        ScreenSize,
        HasCamera,
        DeviceDescription,

        AppName = 20,
        AppPath,
        AppVersion,
        AppBuiltTime,
        AppDbVersion,
        MandantID,
        AppStartTime,
		AppDataPath,
		AppDataTotalUsedBytes,

        FrameworkName = 40,
        FrameworkVersion,
        FrameworkBuiltTime,
        
        FrameworkDeviceFileName = 50,
        FrameworkDeviceVersion,
        FrameworkDeviceBuiltTime,

		FrameworkPlatformFileName = 60,
		FrameworkPlatformVersion,
		FrameworkPlatformBuiltTime,

		DbType = 70,
        DbVersion,
        DbCreateTime,
		DbServer,
		DbName,
		DbUsername,
		DbConnectionString,
		DbVersionLong,
        DbFileDataSize,
        DbFileLogSize,
        DbRowCountAllTables,
        DbBackupFile,
        DbRestoreTime,
        DbRestoreSyncTime,

        MemoryTotalPhysical = 100,
        MemoryAvailablePhysical,
        MemoryTotalVirtual,
        MemoryAvailableVirtual,
        
        StorageRootTotalBytes = 110,
        StorageRootTotalFreeBytes,
        StorageCard1Name,
        StorageCard1TotalBytes,
        StorageCard1TotalFreeBytes,
        StorageCard2Name,
        StorageCard2TotalBytes,
        StorageCard2TotalFreeBytes,
        StorageCard3Name,
        StorageCard3TotalBytes,
        StorageCard3TotalFreeBytes,
        StorageCard4Name,
        StorageCard4TotalBytes,
        StorageCard4TotalFreeBytes,

        ConnectionName = 130,
        Provider,
        PhoneNr,
        IMSI,
        IMEI,
        APN,
        SimType, 
        ConnectionUserName,
        ConnectionUserModified,

		Manufacturer = 150,
		Model,
		NetworkName,
		SystemUserName,

		BiosID = 160,
		BaseID,
		CpuID,
		DiskID,
		VideoID,
        NetworkMacID,

        MotorolaEMDKVersion = 200,
        TeleDriveAppMgrVersion = 201,
        ScanClientVersion = 202


        /*
		 * 
		 * Eingestellte Zeitzone
		 * Navigation (Name, Version, Installpfad)
		 * Welche Version des Motorla-SDK's ist installiert
		 * 
         * AppVersion
         * FrameworkVersion
         * .net CLR Version
         * SQL Version
         * Name der verwendeten FrameworkDevice-DLL + Version
         * DeviceName
         * PhoneNumber
         * Ram-Größe
         * Kamera vorhanden
         * Storage-Card vorhanden bzw wenn vorhanden, Name im FileSystem
         * Name vom internen Speicher
         * Freier Speicher \
         * Freier Speicher StorageCard
         * Freier Speicher interner FlashSpeicher
         * Verzeichnis (Programmpfad von dem das Programm ausgeführt wird - ausführbare Datei)
         * Verzeichnis (Installationspfad von dem das Programm installiert wurde)
         * Anmeldename am PC
         * WLan vorhanden? / aktiv?

        */
    }
}
