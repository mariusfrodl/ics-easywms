﻿using System;
using abmsoft.Mobile.Core.ViewModels;
using abmsoft.Mobile.Core.Layouts;
using Xamarin.Forms;

namespace abmsoft.Mobile.Core.ViewModels
{
	public class CarouselItemViewModelBase : ViewModelBase, ITabProvider
	{
		public CarouselItemViewModelBase ()
		{
		}

		public Color Background { get; set; }
		public string ImageSource { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
	}

}