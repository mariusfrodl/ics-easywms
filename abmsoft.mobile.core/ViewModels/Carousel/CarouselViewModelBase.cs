﻿using System;
using System.Collections.Generic;
using System.Linq;
using abmsoft.Mobile.Core.ViewModels;
using Xamarin.Forms;
using System.Windows.Input;
using System.Diagnostics;

namespace abmsoft.Mobile.Core.ViewModels
{
	public class CarouselViewModelBase : ViewModelBase
	{


		public CarouselViewModelBase()
		{
			CarouselPages = new List<CarouselItemViewModelBase>() {};


			SwitchCarouselForwardCommand = new Command (SwitchCarouselForward);
			SwitchCarouselBackwardCommand = new Command (SwitchCarouselBackward);
		}

		IEnumerable<CarouselItemViewModelBase> _pages;
		public IEnumerable<CarouselItemViewModelBase> CarouselPages {
			get {
				return _pages;
			}
			set {
				SetProperty (ref _pages, value);
				CurrentCarouselPage = CarouselPages.FirstOrDefault ();

			}
		}

		CarouselItemViewModelBase _currentPage;
		public CarouselItemViewModelBase CurrentCarouselPage {
			get {
				return _currentPage;
			}
			set {
				SetProperty (ref _currentPage, value);
			}
		}


		private int _selectedIndex;
		public int SelectedIndex {
			get {
				return _selectedIndex;
			}
			set {
				SetProperty (ref _selectedIndex, value);
			}
		}


		private int _scrollToIndex;
		public int ScrollToIndex {
			get {
				return _scrollToIndex;
			}
			set {
				SetProperty (ref _scrollToIndex, value);
			}
		}

		private void SwitchCarousel(int step) {


			if (step<0 && SelectedIndex == 0) {
				// Directly back to start
				CurrentCarouselPage = CarouselPages.LastOrDefault ();
			} 
			else if (step>0 && SelectedIndex == CarouselPages.Count () - 1) {
				// Directly back to start
				CurrentCarouselPage = CarouselPages.FirstOrDefault ();
			} 
			else {
				// Animated Scrolling
				ScrollToIndex = SelectedIndex + step;
			}

		}

		public ICommand SwitchCarouselForwardCommand { get; set;}

		private void SwitchCarouselForward() {
			SwitchCarousel (1);
		}

		public ICommand SwitchCarouselBackwardCommand { get; set;}

		private void SwitchCarouselBackward() {
			SwitchCarousel (-1);
		}

	}
}

