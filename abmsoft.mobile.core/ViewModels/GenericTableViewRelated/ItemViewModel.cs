﻿using System;
using abmsoft.Mobile.Core.Interfaces;
using Xamarin.Forms;
using System.Windows.Input;

namespace abmsoft.Mobile.Core.ViewModels
{
	public class ItemViewModel<T> : ViewModelBase, IItemViewModel
	{
		public ItemViewModel(string title, Action<T> action, T item, string show)
		{
			Title = title;
			Item = item;
            ShowForm = show;
			Command = new Command<T>(action);
		}

		public ICommand Command { get; set; }

		public T Item { get; set; }
	}
}

