﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace abmsoft.Mobile.Core.ViewModels
{
	public class GroupViewModel<TKey, TItem> : ObservableCollection<TItem>
	{
		public TKey Key { get; private set; }

		public GroupViewModel(TKey key, IEnumerable<TItem> items)
		{
			Key = key;
			if (items != null)
				foreach (var item in items)
					Items.Add(item);
		}
	}}

