﻿using System;
using System.Collections.Generic;

namespace abmsoft.Mobile.Core.ViewModels
{
	public class ItemGroupsViewModel : ViewModelBase 
	{
		public ItemGroupsViewModel(string title, string show)
		{
			Title = title;
            ShowForm = show;
		}

		public IEnumerable<ItemGroupViewModel> Groups { get; set; }
	}
}

