﻿from:
http://adventuresinxamarinforms.com/2015/03/10/grouping-with-listview-and-tableview-and-the-tableviewitemssourcebehavior/

Now in my MountainAreaViewModel all I need to do is to create a list of options, which I can display when a mountain area is selected.


public ItemsViewModel Options { get; set; }
 
private void Initialise()
{
    var groups = new List<ItemGroupViewModel>
    {
        new ItemGroupViewModel("Forecasts")
        {
            new ItemViewModel<ForecastReportViewModel>("Met Office 5 day forecast", ShowForecast, ForecastReport),
            new ItemViewModel<SummitForecastViewModel>("Met Office Summit Forecasts", ShowSummitForecast, SummitForecast),
            new ItemViewModel<MWISForecastViewModel>("MWIS Weather Forecast", ShowMWISForecast, MWISForecast)
        },
        new ItemGroupViewModel("Other")
        {
            new ItemViewModel<AvalancheReportViewModel>("Sais Avalanche Report", ShowAvalancheForecast, AvalancheReport),
            new ItemViewModel<AreaInfoViewModel>("Area Info", ShowAreaInfo, AreaInfo)
        }
    };
 
    var locations = _observationService.GetAreaObservationLocations(_location.Id);
    if (locations != null)
    {
        groups.Insert(1, new ItemGroupViewModel("Weather Stations", 
            locations.Select(location => new ItemViewModel<ObservationLocation>(location.Name,
            ShowObservations, location))));
    }
 
    Options = new ItemGroupsViewModel("Options") { Groups = groups };
}


Assume here that my observationService returns a list of weather observation locations, which I then use to create a grouped list of weather stations. 
Notice that each item calls one of the methods, ShowForecast, ShowObservations etc, which are passed as an action to the ItemViewModel.

And finally I create a view displaying a ListView with its ItemSource bound to the Options property and ground by Key.


<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:views="clr-namespace:Silkweb.Mobile.Core.Views;assembly=Silkweb.Mobile.Core"
             x:Class="Silkweb.Mobile.MountainWeather.Views.ItemsView"
             Title="{Binding Title}">
  <ListView ItemsSource="{Binding Groups}" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand"
            IsGroupingEnabled="True"
            GroupDisplayBinding="{Binding Key}">
    <ListView.ItemTemplate>
      <DataTemplate>
        <views:TextCellExtended Text="{Binding Title}"
                                ShowDisclosure="True" Command="{Binding Command}" CommandParameter="{Binding Item}" />
      </DataTemplate>
    </ListView.ItemTemplate>
  </ListView>
</ContentPage>