﻿using System;
using abmsoft.Mobile.Core.Interfaces;
using System.Collections.Generic;

namespace abmsoft.Mobile.Core.ViewModels
{
	public class ItemGroupViewModel : GroupViewModel<string, IItemViewModel>
	{
		public ItemGroupViewModel(string key, IEnumerable<IItemViewModel> items = null)
			: base(key, items)
		{
		}
	}
}

