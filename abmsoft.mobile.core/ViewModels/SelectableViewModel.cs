﻿using abmsoft.Mobile.Core.ViewModels;
using abmsoft.Mobile.Core.Interfaces;

namespace abmsoft.Mobile.Core.ViewModels
{
    public class SelectableViewModel : ViewModelBase, ISelectable
    {
        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set  {  SetProperty(ref _isSelected, value); }
        }
    }
}

