﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using abmsoft.Mobile.Core.Interfaces;

namespace abmsoft.Mobile.Core.ViewModels
{
	public abstract class ViewModelBase : IViewModel, IDisposable
	{
        private string _title;
        private string _showForm;
        private bool _isBusy;

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public string ShowForm
        {
            get { return _showForm; }
            set { SetProperty(ref _showForm, value); }
        }


        public bool IsBusy
		{
			get { return _isBusy; }
			set { SetProperty(ref _isBusy, value); }
		}

		private bool _IsRefreshing;
		public bool IsRefreshing {
			get {
				return _IsRefreshing;
			}
			set { SetProperty (ref _IsRefreshing, value);}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
		{
			if (object.Equals(storage, value)) return false;

			storage = value;
			OnPropertyChanged(propertyName);

			return true;
		}

		protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var eventHandler = PropertyChanged;
			if (eventHandler != null)
			{
				eventHandler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		protected void OnPropertyChanged<T>(Expression<Func<T>> propertyExpression)
		{
			var propertyName = PropertySupport.ExtractPropertyName(propertyExpression);
			OnPropertyChanged(propertyName);
		}

		public virtual void NavigatedTo()
		{
		}

		public virtual void NavigatedFrom()
		{
		}

		public virtual void Dispose()
		{
		}
	}
}

