﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace abmsoft.Mobile.Core.Views
{
	public partial class KeyValueViewCell : ViewCell
	{
		public static readonly BindableProperty LabelProperty =
			BindableProperty.Create<KeyValueViewCell, string>(
				cell => cell.Label, default(string));

		public static readonly BindableProperty LabelWidthProperty =
			BindableProperty.Create<KeyValueViewCell, int>(
				cell => cell.LabelWidth, default(int));
		
		public static readonly BindableProperty TextProperty = 
			BindableProperty.Create<KeyValueViewCell, string> 
		(cell => cell.Text, default(string));


		public KeyValueViewCell()
		{
			InitializeComponent();
			LabelWidth = 100;  // Default


		}

		public IList<IGestureRecognizer> GestureRecognizers {
			get { return base.View.GestureRecognizers;}
		}

		public int LabelWidth
		{
			set { SetValue(LabelWidthProperty, value); }
			get { return (int)GetValue(LabelWidthProperty); }
		}

		public string Label
		{
			set { SetValue(LabelProperty, value); }
			get { return (string)GetValue(LabelProperty); }
		}

		public string Text
		{
			get { return (string)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}


	}
}

