﻿using System;
using Xamarin.Forms;
using System.Windows.Input;

namespace abmsoft.Mobile.Core.Views
{
	// Erweiterung des XF-ListView um ein Bindable ItemClickCommand
	// siehe
	// http://stackoverflow.com/questions/26040652/binding-to-listview-item-tapped-property-from-view-model
	//
	// Verwendung im XAML:
	// <ContentPage ...
	// xmlns:lv="clr-namespace:abmsoft.Mobile.Core.Views.Views;assembly=abmsoft.Mobile.Core">
	//
	//	<lv:ListView ItemClickCommand="{Binding SelectCommand}" 
	//	ItemsSource="{Binding List}">
	//
	// Im ViewModel ist das Command zu implementieren:
	//	private Command<WordPressPostViewModel> showPostDetailCmd;
	//	public Command<WordPressPostViewModel> ShowPostDetailCommand {
	//		get {
	//			this.showPostDetailCmd = this.showPostDetailCmd ?? new Command<WordPressPostViewModel>(s => 
	//				// z.B. Detailansicht laden
	//				s.ShowPostDetail()
	//			);
	//			return this.showPostDetailCmd;
	//		}
	//	}

	public class xListView : Xamarin.Forms.ListView {

		public static BindableProperty ItemClickCommandProperty = BindableProperty.Create<xListView, ICommand>(x => x.ItemClickCommand, null);


		public xListView() : base() {
			this.ItemTapped += this.OnItemTapped;
		}


		public ICommand ItemClickCommand {
			get { return (ICommand)this.GetValue(ItemClickCommandProperty); }
			set { this.SetValue(ItemClickCommandProperty, value); }
		}


		private void OnItemTapped(object sender, ItemTappedEventArgs e) {
			if (e.Item != null && this.ItemClickCommand != null && this.ItemClickCommand.CanExecute(e)) {
				this.ItemClickCommand.Execute(e.Item);
				this.SelectedItem = null;
			}
		}
	}}

