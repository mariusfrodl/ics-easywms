﻿using System;
using Xamarin.Forms;
using abmsoft.Mobile.Core.Extensions;
using System.Diagnostics;

namespace abmsoft.Mobile.Core.Views
{

	public class PinchToZoomContainer : ContentView
	{
		double startScale;
		double currentScale;
		double yOffset;
		double xOffset;

		public PinchToZoomContainer ()
		{
			var pinchGesture = new PinchGestureRecognizer ();
			pinchGesture.PinchUpdated += OnPinchUpdated;
			GestureRecognizers.Add (pinchGesture);

			// Set PanGestureRecognizer.TouchPoints to control the
			// number of touch points needed to pan
			var panGesture = new PanGestureRecognizer ();
			panGesture.PanUpdated += OnPanUpdated;
			GestureRecognizers.Add (panGesture);
		}


		public void ResetContainer() {
			return;

			startScale = 1;
			currentScale = startScale;
			yOffset = 0;
			xOffset = 0;
			Content.AnchorX = 0;
			Content.AnchorY = 0;

			// Apply translation based on the change in origin.
			Content.TranslationX = 0;
			Content.TranslationY = 0;

			// Apply scale factor.
			Content.Scale = startScale;

		}

		void OnPinchUpdated (object sender, PinchGestureUpdatedEventArgs e)
		{
			if (e.Status == GestureStatus.Started) {
				// Store the current scale factor applied to the wrapped user interface element,
				// and zero the components for the center point of the translate transform.
				startScale = Content.Scale;
				Content.AnchorX = 0;
				Content.AnchorY = 0;
			}
			if (e.Status == GestureStatus.Running) {
				// Calculate the scale factor to be applied.
				currentScale += (e.Scale - 1) * startScale;
				currentScale = Math.Max (1, currentScale);

				// The ScaleOrigin is in relative coordinates to the wrapped user interface element,
				// so get the X pixel coordinate.
				double renderedX = Content.X + xOffset;
				double deltaX = renderedX / Width;
				double deltaWidth = Width / (Content.Width * startScale);
				double originX = (e.ScaleOrigin.X - deltaX) * deltaWidth;

				// The ScaleOrigin is in relative coordinates to the wrapped user interface element,
				// so get the Y pixel coordinate.
				double renderedY = Content.Y + yOffset;
				double deltaY = renderedY / Height;
				double deltaHeight = Height / (Content.Height * startScale);
				double originY = (e.ScaleOrigin.Y - deltaY) * deltaHeight;

				// Calculate the transformed element pixel coordinates.
				double targetX = xOffset - (originX * Content.Width) * (currentScale - startScale);
				double targetY = yOffset - (originY * Content.Height) * (currentScale - startScale);

				// Apply translation based on the change in origin.
				Content.TranslationX = targetX.Clamp (-Content.Width * (currentScale - 1), 0);
				Content.TranslationY = targetY.Clamp (-Content.Height * (currentScale - 1), 0);

				// Apply scale factor.
				Content.Scale = currentScale;
			}
			if (e.Status == GestureStatus.Completed) {
				// Store the translation delta's of the wrapped user interface element.
				xOffset = Content.TranslationX;
				yOffset = Content.TranslationY;
			}
		}

		void OnPanUpdated (object sender, PanUpdatedEventArgs e)
		{
			switch (e.StatusType) {
			case GestureStatus.Running:
				// Translate and ensure we don't pan beyond the wrapped user interface element bounds.
				Content.TranslationX =
					Math.Min (0, xOffset + e.TotalX);
				Content.TranslationY =
					Math.Min (0, yOffset + e.TotalY);
				break;

			case GestureStatus.Completed:
				// Store the translation applied during the pan
				xOffset = Content.TranslationX;
				yOffset = Content.TranslationY;
				break;
			}
		}

		void OnPinchUpdated2 (object sender, PinchGestureUpdatedEventArgs e)
		{
			if (e.Status == GestureStatus.Started) {
				// Store the current scale factor applied to the wrapped user interface element,
				// and zero the components for the center point of the translate transform.
				startScale = Content.Scale;
//				Content.AnchorX = 0;
//				Content.AnchorY = 0;
//				Content.AnchorX = e.ScaleOrigin.X / (Content.Width * startScale);
//				Content.AnchorY = e.ScaleOrigin.Y / (Content.Height * startScale);

				// Calculate the scale factor to be applied.
				currentScale += (e.Scale - 1) * startScale;
				currentScale = Math.Max (1, currentScale);

				// The ScaleOrigin is in relative coordinates to the wrapped user interface element,
				// so get the X pixel coordinate.
				double renderedX = Content.X + xOffset;
				double deltaX = renderedX / Width;
				double deltaWidth = Width / (Content.Width * startScale);
				double originX = (e.ScaleOrigin.X - deltaX) * deltaWidth;


				// The ScaleOrigin is in relative coordinates to the wrapped user interface element,
				// so get the Y pixel coordinate.
				double renderedY = Content.Y + yOffset;
				double deltaY = renderedY / Height;
				double deltaHeight = Height / (Content.Height * startScale);
				double originY = (e.ScaleOrigin.Y - deltaY) * deltaHeight;

				Content.AnchorX = (e.ScaleOrigin.X * deltaWidth);
				Content.AnchorY = (e.ScaleOrigin.Y * deltaHeight) ;

			}
			if (e.Status == GestureStatus.Running) {
				
				// Calculate the scale factor to be applied.
				currentScale += (e.Scale - 1) * startScale;
				currentScale = Math.Max (1, currentScale);

				// The ScaleOrigin is in relative coordinates to the wrapped user interface element,
				// so get the X pixel coordinate.
				double renderedX = Content.X + xOffset;
				double deltaX = renderedX / Width;
				double deltaWidth = Width / (Content.Width * startScale);
				double originX = (e.ScaleOrigin.X - deltaX) * deltaWidth;

					
				// The ScaleOrigin is in relative coordinates to the wrapped user interface element,
				// so get the Y pixel coordinate.
				double renderedY = Content.Y + yOffset;
				double deltaY = renderedY / Height;
				double deltaHeight = Height / (Content.Height * startScale);
				double originY = (e.ScaleOrigin.Y - deltaY) * deltaHeight;


				Debug.WriteLine(String.Format("OnPinchUpdated:  originX={0} originY={1}", originX, originY));

				// Calculate the transformed element pixel coordinates.
//				double targetX = xOffset + (originX * Content.Width) * (currentScale - startScale);
//				double targetY = yOffset + (originY * Content.Height) * (currentScale - startScale);
				double targetX = xOffset + (originX * Content.Width) * (currentScale -startScale );
				double targetY = yOffset + (originY * Content.Height) * (currentScale -startScale);


				Debug.WriteLine(String.Format("OnPinchUpdated:  currentScale={0} startScale={1} ", currentScale, startScale));
//				Debug.WriteLine(String.Format("OnPinchUpdated:  targetX={0} targetY={1} xOffset={2} yOffset={3}", targetX, targetY, xOffset, yOffset));

				// Apply translation based on the change in origin.
				//				Content.TranslationX = targetX.Clamp (0, Content.Width * (currentScale - 1));
				//				Content.TranslationY = targetY.Clamp (0, Content.Height * (currentScale - 1));

				Content.TranslationX = targetX.Clamp (-xOffset, Content.Width * (currentScale - 1));
				Content.TranslationY = targetY.Clamp (-yOffset, Content.Height * (currentScale - 1));

				// Apply scale factor.
				Content.Scale = currentScale;
			}
			if (e.Status == GestureStatus.Completed) {
				// Store the translation delta's of the wrapped user interface element.
				xOffset = Content.TranslationX;
				yOffset = Content.TranslationY;
			}
		}	
	}
}
