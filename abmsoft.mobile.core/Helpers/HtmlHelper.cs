﻿using System;
using System.Text.RegularExpressions;
using System.Net;

namespace abmsoft.Mobile.Core.Helpers
{
	public static class HtmlHelper
	{

		public static string DecodeAndScrubHtml(string html) {
			
			var originalString = WebUtility.HtmlDecode (html);
			originalString = HtmlHelper.ScrubHtml(originalString);
			originalString=originalString.Replace("&#8220;","\"").Replace("&#8221;","\"").Replace("&#160;"," ");
			return originalString;
		}
	
		public static string ScrubHtml(string value) {
			var step1 = Regex.Replace(value, @"<[^>]+>|&nbsp;", "").Trim();
			var step2 = Regex.Replace(step1, @"\s{2,}", " ");
			return step2;
		}
	
	
	
		public static string UrlEncode(string url) {
			return WebUtility.UrlEncode (url);
		}
	}



}

