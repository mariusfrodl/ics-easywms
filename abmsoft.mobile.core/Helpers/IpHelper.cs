﻿using System;

namespace abmsoft.Mobile.Core.Helpers
{
	public static class IpHelper
	{
		public static bool ValidateIpAdress(string ipInput) {

			if (String.IsNullOrEmpty (ipInput))
				return false;

			string[] IpAndPort = ipInput.Split (':');

			var Ip = IpAndPort [0];
			var ipParts = Ip.Split('.');

			if (ipParts.Length == 0 || ipParts.Length>4)
				return false;


			foreach (var part in ipParts) {
				int val = -1;
				if (int.TryParse (part, out val)) {
					if (val < 0 || val > 255)
						return false;
				} else
					return false;
			}

			if (ipParts.Length != 4)
				return false;

			if (IpAndPort.Length == 2) {
				var portInput = IpAndPort [1];
				int port = -1;
				if (int.TryParse (portInput, out port)) {
					if (port < 1 || port > 65535)
						return false;
				} else
					return false;
			}

			return true;

		}	}
}

