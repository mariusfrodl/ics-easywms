﻿using System;
using Xamarin.Forms;

namespace abmsoft.Mobile.Core.Helpers
{
	public static class EasingHelper
	{
		public static Easing GetEasing(string easingName)
		{
			switch (easingName)
			{
			case "BounceIn": return Easing.BounceIn;
			case "BounceOut": return Easing.BounceOut;
			case "CubicInOut": return Easing.CubicInOut;
			case "CubicOut": return Easing.CubicOut;
			case "Linear": return Easing.Linear;
			case "SinIn": return Easing.SinIn;
			case "SinInOut": return Easing.SinInOut;
			case "SinOut": return Easing.SinOut;
			case "SpringIn": return Easing.SpringIn;
			case "SpringOut": return Easing.SpringOut;
			default: throw new ArgumentException(easingName + " is not a valid Easing function");
			}
		}	
	}
}

