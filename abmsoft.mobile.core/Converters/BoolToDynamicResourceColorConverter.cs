﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace abmsoft.Mobile.Core.Converters
{
	public class BoolToDynamicResourceColorConverter : IValueConverter
	{
		public string TrueColor { set; get; }

		public string FalseColor { set; get; }

		public object Convert(object value, Type targetType, 
			object parameter, CultureInfo culture)
		{
			return (bool)value ? (Color)Application.Current.Resources[TrueColor] : (Color)Application.Current.Resources[FalseColor];
		}

		public object ConvertBack(object value, Type targetType, 
			object parameter, CultureInfo culture)
		{
			return false;
		}
	}
}
