﻿using System;
using Xamarin.Forms;
using System.Net;
using System.Globalization;

namespace abmsoft.Mobile.Core.Converters
{
	public class StringHtmlDecodeConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is String) || !(parameter is String))
				return null;

			var originalString = WebUtility.HtmlDecode ((String)value);
			return originalString.Replace("&#8220;","\"").Replace("&#8221;","\"");
				
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return null;
		}	

	}
}

