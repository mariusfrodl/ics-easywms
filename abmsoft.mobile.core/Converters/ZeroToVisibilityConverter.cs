﻿
using System;
using System.Globalization;
using Xamarin.Forms;

namespace abmsoft.Mobile.Core.Converters
{
	public class ZeroToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return  (double?)value != null && (double?)value != 0;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}