﻿using System;
using System.Globalization;
using Xamarin.Forms;
using System.Diagnostics;

namespace abmsoft.Mobile.Core.Converters
{
    public class BoolToColorConverter : IValueConverter
    {
        public Color TrueColor { set; get; }

        public Color FalseColor { set; get; }

        public object Convert(object value, Type targetType, 
                              object parameter, CultureInfo culture)
        {
			Debug.WriteLine (TrueColor);
            return (bool)value ? TrueColor : FalseColor;
        }

        public object ConvertBack(object value, Type targetType, 
                                  object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}
