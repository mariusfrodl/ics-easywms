﻿using System;
using Xamarin.Forms;
using System.Globalization;
using System.Text.RegularExpressions;

namespace abmsoft.Mobile.Core.Converters
{
	public class StringToSubstringConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is String) || !(parameter is String))
				return null;

			var originalString = ScrubHtml((String)value);
			originalString=originalString.Replace("&#8220;","\"").Replace("&#8221;","\"");

			var parameters = ((string)parameter).Split (',');

			var offset = 0;
			var length = originalString.Length;

			if (parameters.Length == 2) {
				offset = int.Parse (parameters [0]);
				length = Math.Min (int.Parse (parameters [1]), length-offset);
			} else {
				length = int.Parse (parameters [0]);
			}
				
				
			if (originalString.Length < length)
				return originalString;
			else
				return originalString.Substring (offset,length) + "...";
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return null;
		}	

		private string ScrubHtml(string value) {
			var step1 = Regex.Replace(value, @"<[^>]+>|&nbsp;", "").Trim();
			var step2 = Regex.Replace(step1, @"\s{2,}", " ");
			return step2;
		}
	}
}

