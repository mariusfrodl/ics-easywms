﻿using System;
using Xamarin.Forms;
using System.Globalization;
using System.Net;

namespace abmsoft.Mobile.Core.Converters
{
	public class HtmlSourceConverter : IValueConverter
	{
		public String Replace { set; get; }

		public String By { set; get; }

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{

			bool htmldecode = false;
			string baseUrl = String.Empty;

			if (!(value is String))
				return null;

			// Check if there is a HtmlDecode-Parameter (true/false)
			if (parameter != null) {
				var parameters = ((string)parameter).Split (',');

				if (parameters.Length >= 1) {
					htmldecode = bool.Parse (parameters [0]);
				}

				if (parameters.Length >= 2)
					baseUrl = (string)parameters [1];
					
			}

			var htmlString = (String)value;

			if (htmldecode)
				htmlString = WebUtility.HtmlDecode ((String)value);

			if (!String.IsNullOrEmpty (Replace) && !String.IsNullOrEmpty (By))
				htmlString = htmlString.Replace (Replace, By);

			//htmlString=htmlString.Replace("&#8220;","\"").Replace("&#8221;","\"");


			var html = new HtmlWebViewSource();
			if (!String.IsNullOrEmpty(baseUrl))
				html.BaseUrl = baseUrl;
		

			if (value != null)
			{
				html.Html = htmlString.ToString();
			}

			return html;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}

