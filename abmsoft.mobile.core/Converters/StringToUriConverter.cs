﻿using System;
using Xamarin.Forms;

namespace abmsoft.Mobile.Core.Converters
{
	public class StringToUriConverter : IValueConverter
	{
		public string Prefix { get; set;}

		#region IValueConverter implementation

		object IValueConverter.Convert (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{

			if (!(value is String))
				return null;

			string Url = value as String;
			if (!String.IsNullOrEmpty (Prefix))
				Url = Prefix + Url;

			Uri src = new Uri (Url);
			return src;

		}

		object IValueConverter.ConvertBack (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException ();
		}

		#endregion
	}
}

