﻿using System;
using Xamarin.Forms;
using System.Globalization;

namespace abmsoft.Mobile.Core.Converters
{
	public class BoolToStyleConverter : IValueConverter
	{
		public Style TrueStyle { set; get; }

		public Style FalseStyle { set; get; }

		public object Convert(object value, Type targetType, 
			object parameter, CultureInfo culture)
		{
			return (bool)value ? TrueStyle : FalseStyle;
		}

		public object ConvertBack(object value, Type targetType, 
			object parameter, CultureInfo culture)
		{
			return false;
		}	
	}
}

