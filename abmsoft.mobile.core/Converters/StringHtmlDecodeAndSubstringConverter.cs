﻿using System;
using Xamarin.Forms;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Net;
using abmsoft.Mobile.Core.Helpers;

namespace abmsoft.Mobile.Core.Converters
{
	public class StringHtmlDecodeAndScrubConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is String) || !(parameter is String))
				return null;

			var originalString = HtmlHelper.DecodeAndScrubHtml ((String)value);

			var parameters = ((string)parameter).Split (',');

			var offset = 0;
			var length = originalString.Length;

			if (parameters.Length == 2) {
				offset = int.Parse (parameters [0]);
				length = Math.Min (int.Parse (parameters [1]), length-offset);
			} else {
				length = int.Parse (parameters [0]);
			}


			if (originalString.Length < length)
				return originalString;
			else
				return originalString.Substring (offset,length) + "...";
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return null;
		}	


	}
}

