﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace abmsoft.Mobile.Core.Converters
{
	/// <summary>
	/// Null to is visible converter.
	/// Can be used to make a control visible when a Property is null.
	/// (NullToVisibilityConverter does the opposite)
	/// </summary>
	public class NullToIsVisibleConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return value == null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}

