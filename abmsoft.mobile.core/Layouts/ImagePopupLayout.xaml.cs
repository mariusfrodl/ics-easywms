﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading.Tasks;
using abmsoft.Mobile.Core.Helpers;

namespace abmsoft.Mobile.Core.Layouts
{
	public partial class ImagePopupLayout : ContentView
	{
		public ImagePopupLayout ()
		{
			InitializeComponent ();
			this.IsVisible = false;
		}


		public static readonly BindableProperty PopupImageSourceProperty =
			BindableProperty.Create<ImagePopupLayout, string> (
				ctrl => ctrl.PopupImageSource,
				default(string),
				BindingMode.OneWay,
				propertyChanged: (bindable, oldValue, newValue) => {
					((ImagePopupLayout)bindable).SetPopupImageSource(newValue);

				}

			);

		public string PopupImageSource {
			get {
				return (string)GetValue (PopupImageSourceProperty);
			}
			set {
				SetValue (PopupImageSourceProperty, value);
			}
		}

		private void SetPopupImageSource(string src) {
			PopupImage.Source = src;
		}


		public static readonly BindableProperty RotateToProperty =
			BindableProperty.Create<ImagePopupLayout, double> (
				ctrl => ctrl.RotateTo,
				default(double),
				BindingMode.TwoWay
			);

		public double RotateTo {
			get {
				return (double)GetValue (RotateToProperty);
			}
			set {
				SetValue (RotateToProperty, value);
			}
		}


		public static readonly BindableProperty AnimationDurationProperty =
			BindableProperty.Create<ImagePopupLayout, int> (
				ctrl => ctrl.AnimationDuration,
				default(int),
				BindingMode.TwoWay
			);

		public int AnimationDuration {
			get {
				return (int)GetValue (AnimationDurationProperty);
			}
			set {
				SetValue (AnimationDurationProperty, value);
			}
		}


		public static readonly BindableProperty EasingProperty =
			BindableProperty.Create<ImagePopupLayout, string> (
				ctrl => ctrl.PopupEasing,
				Easing.Linear.ToString(),
				BindingMode.TwoWay,
				propertyChanged: (bindable, oldValue, newValue) => {

				}
			);

		public string PopupEasing {
			get {
				return (string)GetValue (EasingProperty);
			}
			set {
				SetValue (EasingProperty, value);
			}
		}

		public async void PopupCloseButtonClicked(object sender, EventArgs e) {
			await ClosePopup ();
		}

		public async void OnPopupImageTapped(object sender, EventArgs e) {
			await ClosePopup ();
		}

		private async Task ClosePopup() {
			Easing ease = EasingHelper.GetEasing (PopupEasing);
			this.RotateTo (0, (uint)this.AnimationDuration);
			await this.ScaleTo (0, (uint)this.AnimationDuration, ease);
			this.IsVisible = false;
		}

		/// <summary>
		/// Activates the popup. Has to be called from the Page.
		/// </summary>
		public async Task ActivatePopup() {

			Easing ease = EasingHelper.GetEasing (PopupEasing);
			this.Scale = 0;
			PopupLayer.BackgroundColor = this.BackgroundColor;
			this.IsVisible = true;
			this.RotateTo (this.RotateTo, (uint)this.AnimationDuration);
			await this.ScaleTo (1, (uint)this.AnimationDuration, ease);
		}



	}
}

