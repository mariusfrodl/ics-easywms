﻿using System;

namespace abmsoft.Mobile.Core.Layouts
{
	/// <summary>
	/// ITab provider.
	/// http://chrisriesgo.com/xamarin-forms-carousel-view-recipe/
	/// </summary>
	public interface ITabProvider
	{
		string ImageSource { get; set; }
	}

}

