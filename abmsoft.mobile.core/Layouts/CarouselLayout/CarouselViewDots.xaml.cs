﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections;
using System.Windows.Input;
using System.Diagnostics;
using System.Threading.Tasks;

namespace abmsoft.Mobile.Core.Layouts
{
	public partial class CarouselViewDots : ContentView
	{
		public CarouselViewDots ()
		{
			InitializeComponent ();
		}
			
		private Color _dotColor = Color.Black;
		public Color DotColor {
			get { return _dotColor;}
			set { _dotColor = value;
				dotLayout.DotColor = value;

			}
		}

		private double _dotSize =5;
		public double DotSize {
			get { return _dotSize;}
			set { _dotSize = value;
				dotLayout.DotSize = value;

			}
		}

		private DataTemplate _itemTemplate;
		public DataTemplate ItemTemplate {
			get { return _itemTemplate;}
			set { 

				_itemTemplate=value;
				carouselLayout.ItemTemplate = value;
			}
		}


		public int SelectedIndex {
			get {
				return (int)carouselLayout.SelectedIndex;
			}
		}


		public static readonly BindableProperty ScrollToIndexProperty =
			BindableProperty.Create<CarouselViewDots, int> (
				carousel => carousel.ScrollToIndex,
				0,
				BindingMode.TwoWay,
				propertyChanged: async (bindable, oldValue, newValue) => {
					await ((CarouselViewDots)bindable).ScrollToIndexCommand (newValue);

				}
			);

		public int ScrollToIndex {
			get {
				return (int)GetValue (ScrollToIndexProperty);
			}
			set {
				SetValue (ScrollToIndexProperty, value);
			}
		}
			

		public async Task ScrollToIndexCommand (int index) {
			var el = carouselLayout.Children[index];
			carouselLayout.ScrollToIndex = index;

			bool animated = true;

			// Disable animation on Android as it then always starts from the first item
			// No more needed, as of XF 1.5.1
			// Device.OnPlatform (Android: () => animated = false);
			await carouselLayout.ScrollToAsync (el, ScrollToPosition.MakeVisible, animated);

			// Update SelectedIndex manually as it isn't updated by the Android Renderer when scrolling is not manually
			carouselLayout.SelectedIndex = index;
		}
	}
}

