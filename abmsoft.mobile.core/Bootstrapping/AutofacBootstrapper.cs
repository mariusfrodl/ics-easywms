﻿using System;
using Autofac;
using abmsoft.Mobile.Core.Factories;
using abmsoft.Mobile.Core.Interfaces;

namespace abmsoft.Mobile.Core.Bootstrapping
{
	public abstract class AutofacBootstrapper
	{
		public void Run()
		{

			var builder = new ContainerBuilder();

			ConfigureContainer(builder);

			var container = builder.Build();
			var viewFactory = container.Resolve<IViewFactory>();

			RegisterViews(viewFactory);

			ConfigureApplication(container);
		}

		protected virtual void ConfigureContainer(ContainerBuilder builder)
		{
			builder.RegisterModule<AutofacModule>();
		}

		protected abstract void RegisterViews(IViewFactory viewFactory);

		protected abstract void ConfigureApplication(IContainer container);
	}

}

