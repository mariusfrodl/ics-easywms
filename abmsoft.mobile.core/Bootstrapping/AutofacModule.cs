﻿using System;
using Autofac;
using abmsoft.Mobile.Core.Factories;
using abmsoft.Mobile.Core.Services;
using Xamarin.Forms;
using abmsoft.Mobile.Core.Interfaces;
using abmsoft.Mobile.Core.Views;
using System.Diagnostics;

namespace abmsoft.Mobile.Core.Bootstrapping
{
	public class AutofacModule : Module
	{

		protected override void Load(ContainerBuilder builder)
		{
			// service registration
			builder.RegisterType<DialogService>()
				.As<IDialogProvider>()
				.SingleInstance();

			builder.RegisterType<ViewFactory>()
				.As<IViewFactory>()
				.SingleInstance();

			builder.RegisterType<Navigator>()
				.As<INavigator>()
				.SingleInstance();

			// default page resolver
			builder.RegisterInstance<Func<Page>>(() =>
				{
					Debug.WriteLine("Func-Page called: " );
					// Check if we are using MasterDetailPage
					var masterDetailPage = Application.Current.MainPage as MasterDetailPage;
					var tabbedPage = Application.Current.MainPage as TabbedPage;

					Page page;

					if ( masterDetailPage != null )
						page = masterDetailPage.Detail;
					else if (tabbedPage != null) 
						page = tabbedPage.CurrentPage;
					else
						page = Application.Current.MainPage;

					// Check if page is a NavigationPage
					var navigationPage = page as IPageContainer<Page>;

					return navigationPage != null 
						? navigationPage.CurrentPage
							: page;
				}
			);

			// current PageProxy
			builder.RegisterType<PageProxy>()
				.As<IPage>()
				.SingleInstance();
		}
	}

}

