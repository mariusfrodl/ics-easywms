﻿using System;
using System.Threading.Tasks;
using abmsoft.Mobile.Core.Factories;
using abmsoft.Mobile.Core.ViewModels;
using abmsoft.Mobile.Core.Interfaces;
using Xamarin.Forms;
using System.Diagnostics;

namespace abmsoft.Mobile.Core.Services
{
	public class Navigator : INavigator
	{
		private readonly IPage _page;
		private readonly IViewFactory _viewFactory;

		public Navigator(IPage page, IViewFactory viewFactory)
		{
			_page = page;
			_viewFactory = viewFactory;
		}

		private INavigation Navigation
		{
			get { return _page.Navigation; }
		}

		public async Task<IViewModel> PopAsync()
		{
			try {
				Page view = await Navigation.PopAsync();
				var viewModel = view.BindingContext as IViewModel;
				viewModel.NavigatedFrom();
				return viewModel;
			}
			catch(Exception e) {
				Debug.WriteLine ("Exception in PopAsync: " + e.Message);
			}
			return null;
		}

		public async Task<IViewModel> PopModalAsync()
		{
			try {
				Page view = await Navigation.PopModalAsync();
				var viewModel = view.BindingContext as IViewModel;
				viewModel.NavigatedFrom();
				return viewModel;
			}
			catch(Exception e) {
				Debug.WriteLine ("Exception in PopAsync: " + e.Message);
			}
			return null;
		}

		public async Task PopToRootAsync()
		{
			await Navigation.PopToRootAsync();
		}

		public async Task<TViewModel> PushAsync<TViewModel>(Action<TViewModel> setStateAction = null) 
			where TViewModel : class, IViewModel
		{

			try {
				TViewModel viewModel;
				var view = _viewFactory.Resolve<TViewModel>(out viewModel, setStateAction);
				await Navigation.PushAsync(view);
				viewModel.NavigatedTo();
				return viewModel;
			}
			catch(Exception e) {
				Debug.WriteLine ("Exception in PushAsync: " + e.Message);
			}
			return null;
		}

		public async Task<TViewModel> PushAsync<TViewModel>(TViewModel viewModel) 
			where TViewModel : class, IViewModel
		{
			try {
				var view = _viewFactory.Resolve(viewModel);
				await Navigation.PushAsync(view);
				viewModel.NavigatedTo();
				return viewModel;
			}
			catch(Exception e) {
				Debug.WriteLine ("Exception in PushAsync: " + e.Message);
			}
			return null;
		}



		public async Task<TViewModel> PushModalAsync<TViewModel>(Action<TViewModel> setStateAction = null) 
			where TViewModel : class, IViewModel
		{
			try
			{
				TViewModel viewModel;
				var view = _viewFactory.Resolve<TViewModel>(out viewModel, setStateAction);
				await Navigation.PushModalAsync(view);
				return viewModel;
			}
			catch(Exception e) {
				Debug.WriteLine ("Exception in PushModalAsync: " + e.Message);
			}
			return null;

		}

		public async Task<TViewModel> PushModalAsync<TViewModel>(TViewModel viewModel) 
			where TViewModel : class, IViewModel
		{
			try {
				var view = _viewFactory.Resolve(viewModel);
				await Navigation.PushModalAsync(view);
				return viewModel;
			}
			catch(Exception e) {
				Debug.WriteLine ("Exception in PushModalAsync: " + e.Message);
			}
			return null;

		}
			

	}
}

