﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using abmsoft.Mobile.Core.ViewModels;

namespace abmsoft.Mobile.Core.Behaviors
{

//	USAGE SAMPLE:
//  see http://adventuresinxamarinforms.com/2015/03/10/grouping-with-listview-and-tableview-and-the-tableviewitemssourcebehavior/
//
//	<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
//		xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
//		xmlns:views="clr-namespace:Silkweb.Mobile.Core.Views;assembly=Silkweb.Mobile.Core"
//		xmlns:behaviors="clr-namespace:Silkweb.Mobile.Core.Behaviors;assembly=Silkweb.Mobile.Core"
//		x:Class="Silkweb.Mobile.MountainWeather.Views.ItemsView"
//		Title="{Binding Title}">
//
//		<TableView Intent="Menu">
//		<TableView.Behaviors>
//		<behaviors:TableViewItemsSourceBehavior ItemsSource="{Binding Groups}">
//		<behaviors:TableViewItemsSourceBehavior.ItemTemplate>
//		<DataTemplate>
//		<views:TextCellExtended Text="{Binding Title}" ShowDisclosure="True" Command="{Binding Command}" CommandParameter="{Binding Item}" />
//		</DataTemplate>
//		</behaviors:TableViewItemsSourceBehavior.ItemTemplate>
//		</behaviors:TableViewItemsSourceBehavior>
//		</TableView.Behaviors>
//		</TableView>
//
//		</ContentPage>

	public class TableViewItemsSourceBehavior : BindableBehavior<TableView>
	{
		public static readonly BindableProperty ItemsSourceProperty =
			BindableProperty.Create<TableViewItemsSourceBehavior, IEnumerable<ItemGroupViewModel>>(p => p.ItemGroupsSource, null, BindingMode.Default, null, ItemsSourceChanged);

		public IEnumerable<ItemGroupViewModel> ItemGroupsSource
		{
			get { return (IEnumerable<ItemGroupViewModel>)GetValue(ItemsSourceProperty); } 
			set { SetValue(ItemsSourceProperty, value); } 
		}

		public static readonly BindableProperty ItemTemplateProperty =
			BindableProperty.Create<TableViewItemsSourceBehavior, DataTemplate>(p => p.ItemTemplate, null);

		public DataTemplate ItemTemplate
		{ 
			get { return (DataTemplate)GetValue(ItemTemplateProperty); } 
			set { SetValue(ItemTemplateProperty, value); } 
		}

		private static void ItemsSourceChanged(BindableObject bindable, IEnumerable<ItemGroupViewModel> oldValue, IEnumerable<ItemGroupViewModel> newValue)
		{
			var behavior = bindable as TableViewItemsSourceBehavior;
			if (behavior == null) return;
			behavior.SetItems();
		}

		private void SetItems()
		{
			if (ItemGroupsSource == null) return;

			AssociatedObject.Root.Clear();

			foreach (var group in ItemGroupsSource)
			{
				var tableSection = new TableSection { Title = group.Key };

				foreach (var itemViewModel in group)
				{
					var content = ItemTemplate.CreateContent();
					var cell = content as Cell;
					if (cell == null) continue;
					cell.BindingContext = itemViewModel;
					tableSection.Add(cell);
				}

				AssociatedObject.Root.Add(tableSection);
			}
		}
	}
}

